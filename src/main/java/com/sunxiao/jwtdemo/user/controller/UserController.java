package com.sunxiao.jwtdemo.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sunxiao.jwtdemo.exception.CustomException;
import com.sunxiao.jwtdemo.exception.CustomUnauthorizedException;

import com.sunxiao.jwtdemo.superd.spojo.BeanConverts;
import com.sunxiao.jwtdemo.user.pojo.common.Constant;
import com.sunxiao.jwtdemo.user.pojo.common.ResponseBean;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import com.sunxiao.jwtdemo.user.pojo.dto.LoginUserDto;
import com.sunxiao.jwtdemo.user.pojo.dto.UserDto;
import com.sunxiao.jwtdemo.user.pojo.group.UserEditValidGroup;
import com.sunxiao.jwtdemo.user.pojo.group.UserLoginValidGroup;
import com.sunxiao.jwtdemo.user.service.UserService;
import com.sunxiao.jwtdemo.util.*;
import com.sunxiao.jwtdemo.util.common.StringUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.*;


@RequestMapping("/user")
@RestController
public class UserController {
    /**
     * RefreshToken过期时间
     */
    @Value("${refreshTokenExpireTime}")
    private String refreshTokenExpireTime;
    @Autowired
    UserService userService;

    private final UserUtil userUtil;

    public UserController(UserUtil userUtil) {
        this.userUtil = userUtil;
    }


    /**
     * 用户登录
     * @param userDto
     * @param httpServletResponse
     * @return
     */
    @PostMapping("/login")
    public ResponseBean login(@Validated(UserLoginValidGroup.class) @RequestBody LoginUserDto userDto, HttpServletResponse httpServletResponse) {
        // 查询数据库中的帐号信息
        User user = userService.selectOne(new QueryWrapper<User>().eq("account", userDto.getAccount()));
        if (user == null) {
            throw new CustomUnauthorizedException("该帐号不存在(The account does not exist.)");
        }
        String salt = user.getSalt();
        if (AesCipherUtil.enCryptoMd5(userDto.getPassword(),salt).equals(user.getPassword())) {
            if (JedisUtil.exists(Constant.PREFIX_SHIRO_CACHE + userDto.getAccount())) {
                JedisUtil.delKey(Constant.PREFIX_SHIRO_CACHE + userDto.getAccount());
            }
            // 设置RefreshToken，时间戳为当前时间戳，直接设置即可(不用先删后设，会覆盖已有的RefreshToken)
            String currentTimeMillis = String.valueOf(System.currentTimeMillis());
            JedisUtil.setObject(Constant.PREFIX_SHIRO_REFRESH_TOKEN + userDto.getAccount(), currentTimeMillis, Integer.parseInt(refreshTokenExpireTime));
            // 从Header中Authorization返回AccessToken，时间戳为当前时间戳
            String token = JwtUtil.sign(userDto.getAccount(), currentTimeMillis);
            httpServletResponse.setHeader("Authorization", token);
            httpServletResponse.setHeader("Access-Control-Expose-Headers", "Authorization");
            return new ResponseBean(HttpStatus.OK.value(), "登录成功(Login Success.)", token);
        }else {
            throw new CustomUnauthorizedException("帐号或密码错误(Account or Password Error.)");
        }
    }
    @PostMapping("/add")
//    @RequiresPermissions(logical = Logical.AND, value = {"user:edit"})
    public ResponseBean add(@Validated(UserEditValidGroup.class) @RequestBody LoginUserDto userDto) throws UnsupportedEncodingException {
        // 判断当前帐号是否存在
        User user = userService.selectOne(new QueryWrapper<User>().eq("account", userDto.getAccount()));
        if (user != null && StringUtil.isNotBlank(user.getPassword())) {
            throw new CustomUnauthorizedException("该帐号已存在(Account exist.)");
        }
        //密码加密
        String salt = SaltUtils.getSalt(new Random().nextInt(10) + 5);
        String md5hash = AesCipherUtil.enCryptoMd5(userDto.getPassword(), salt);
        User user1 = new User();
        user1.setAccount(userDto.getAccount()).setPassword(md5hash).setSalt(salt);
        int count = userService.insert(user1);
        if (count <= 0) {
            throw new CustomException("新增失败(Insert Failure)");
        }
        return new ResponseBean(HttpStatus.OK.value(), "新增成功(Insert Success)", null);
    }

    /**
     * 获取当前用户
     * @return
     */
    @GetMapping("/info")
    @RequiresAuthentication
    public ResponseBean info() {
        // 获取当前登录用户
        UserDto userDto = userUtil.getUser();
        // 获取当前登录用户Id
        String id = userUtil.getUserId();
        // 获取当前登录用户Token
        String token = userUtil.getToken();
        // 获取当前登录用户Account
        String account = userUtil.getAccount();
        return new ResponseBean(HttpStatus.OK.value(), "您已经登录了(You are already logged in)", userDto);
    }
    /**
     * 获取在线用户(查询Redis中的RefreshToken)
     * @param
     * @return com.wang.model.common.ResponseBean
     * @author dolyw.com
     * @date 2018/9/6 9:58
     */
    @GetMapping("/online")
    @RequiresPermissions(logical = Logical.AND, value = {"user:view"})
    public ResponseBean online() {
        List<Object> userDtos = new ArrayList<Object>();
        // 查询所有Redis键
        Set<String> keys = JedisUtil.keysS(Constant.PREFIX_SHIRO_REFRESH_TOKEN + "*");
        for (String key : keys) {
            if (JedisUtil.exists(key)) {
                // 根据:分割key，获取最后一个字符(帐号)
                String[] strArray = key.split(":");
                User user = userService.selectOne(new QueryWrapper<User>().eq("account", strArray[strArray.length - 1]));
                UserDto convert = BeanConverts.convert(user, UserDto.class);
                userDtos.add(convert);
            }
        }
        if (userDtos == null || userDtos.size() < 0) {
            throw new CustomException("查询失败(Query Failure)");
        }
        return new ResponseBean(HttpStatus.OK.value(), "查询成功(Query was successful)", userDtos);
    }

}
