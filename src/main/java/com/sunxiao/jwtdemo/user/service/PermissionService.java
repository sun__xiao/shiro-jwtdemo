package com.sunxiao.jwtdemo.user.service;

import com.sunxiao.jwtdemo.superd.sservice.StringSuperService;
import com.sunxiao.jwtdemo.user.pojo.domain.Permission;
import org.springframework.stereotype.Service;

@Service
public interface PermissionService extends StringSuperService<Permission> {
}
