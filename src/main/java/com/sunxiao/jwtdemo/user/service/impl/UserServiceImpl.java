package com.sunxiao.jwtdemo.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sunxiao.jwtdemo.superd.smapper.SuperMapper;
import com.sunxiao.jwtdemo.superd.sservice.simpl.StringSuperServiceImpl;
import com.sunxiao.jwtdemo.user.mapper.RoleMapper;
import com.sunxiao.jwtdemo.user.mapper.UserMapper;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import com.sunxiao.jwtdemo.user.pojo.vo.AuthenticationVo;

import com.sunxiao.jwtdemo.user.pojo.vo.RoleData;
import com.sunxiao.jwtdemo.user.pojo.vo.RolePermissionVo;
import com.sunxiao.jwtdemo.user.pojo.vo.UserRoleVo;
import com.sunxiao.jwtdemo.user.service.UserService;
import com.sunxiao.jwtdemo.util.AesCipherUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends StringSuperServiceImpl<User> implements UserService {
    @Autowired
    UserMapper mapper;
    @Autowired
    RoleMapper roleMapper;

    @Override
    public SuperMapper<User> getSuperMapper() {
        return mapper;
    }



    @Override
    public AuthenticationVo findAuthenticationByAccount(String account) {
        AuthenticationVo authenticationVo=new AuthenticationVo();
        authenticationVo.setAccount(account);
        List<RoleData> roleData=new ArrayList<>();
        List<UserRoleVo> roleGroup = mapper.findRoleGroup(account);
        roleGroup.stream().forEach(e->{
            RoleData vo=new RoleData();
            vo.setRolename(e.getRolename());
            List<RolePermissionVo> byRoleName = roleMapper.findByRoleName(e.getRolename());
            vo.setRolePermissionVos(byRoleName);
            roleData.add(vo);
        });
        authenticationVo.setRolenameList(roleData);

        return authenticationVo;
    }
}
