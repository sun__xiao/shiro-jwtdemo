package com.sunxiao.jwtdemo.user.service;

import com.sunxiao.jwtdemo.superd.sservice.StringSuperService;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import com.sunxiao.jwtdemo.user.pojo.vo.AuthenticationVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService extends StringSuperService<User> {

    AuthenticationVo findAuthenticationByAccount(String account);


}
