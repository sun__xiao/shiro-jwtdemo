package com.sunxiao.jwtdemo.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.sunxiao.jwtdemo.superd.smapper.SuperMapper;
import com.sunxiao.jwtdemo.superd.sservice.simpl.StringSuperServiceImpl;
import com.sunxiao.jwtdemo.user.mapper.PermissionMapper;
import com.sunxiao.jwtdemo.user.mapper.RoleMapper;
import com.sunxiao.jwtdemo.user.pojo.domain.Permission;
import com.sunxiao.jwtdemo.user.pojo.domain.Role;
import com.sunxiao.jwtdemo.user.pojo.vo.RolePermissionVo;
import com.sunxiao.jwtdemo.user.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleServiceImpl extends StringSuperServiceImpl<Role> implements RoleService {

    @Autowired
    RoleMapper mapper;
    @Autowired
    PermissionMapper permissionmapper;
    @Override
    public SuperMapper<Role> getSuperMapper() {
        return mapper;
    }

    @Override
    public List<RolePermissionVo> findByRoleName(String roleName) {
        List<RolePermissionVo> byRoleName = mapper.findByRoleName(roleName);
        return null;
    }

}
