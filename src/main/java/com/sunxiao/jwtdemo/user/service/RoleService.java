package com.sunxiao.jwtdemo.user.service;

import com.sunxiao.jwtdemo.superd.sservice.StringSuperService;
import com.sunxiao.jwtdemo.user.pojo.domain.Role;

import com.sunxiao.jwtdemo.user.pojo.vo.RolePermissionVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService extends StringSuperService<Role> {

    List<RolePermissionVo> findByRoleName(String roleName);
}
