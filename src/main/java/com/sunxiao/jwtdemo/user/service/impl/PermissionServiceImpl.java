package com.sunxiao.jwtdemo.user.service.impl;

import com.sunxiao.jwtdemo.superd.smapper.SuperMapper;
import com.sunxiao.jwtdemo.superd.sservice.simpl.StringSuperServiceImpl;
import com.sunxiao.jwtdemo.user.mapper.PermissionMapper;
import com.sunxiao.jwtdemo.user.pojo.domain.Permission;
import com.sunxiao.jwtdemo.user.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl extends StringSuperServiceImpl<Permission> implements PermissionService {

    @Autowired
    PermissionMapper mapper;


    @Override
    public SuperMapper<Permission> getSuperMapper() {
        return mapper;
    }
}
