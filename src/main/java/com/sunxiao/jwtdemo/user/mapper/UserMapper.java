package com.sunxiao.jwtdemo.user.mapper;

import com.sunxiao.jwtdemo.superd.smapper.SuperMapper;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import com.sunxiao.jwtdemo.user.pojo.vo.UserRoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends SuperMapper<User> {
    /**
     * 通过账号查询角色
     * @param account
     * @return
     */
    @Select("SELECT u.account account, r.id roleid,r.name rolename from t_user u left JOIN t_user_role" +
            " ur on ur.user_id=u.id LEFT JOIN t_role r ON ur.role_id=r.id WHERE u.account=#{account}")
    List<UserRoleVo> findRoleGroup(String account);
}
