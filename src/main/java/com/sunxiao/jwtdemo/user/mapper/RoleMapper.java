package com.sunxiao.jwtdemo.user.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sunxiao.jwtdemo.superd.smapper.SuperMapper;
import com.sunxiao.jwtdemo.user.pojo.domain.Role;
import com.sunxiao.jwtdemo.user.pojo.vo.RolePermissionVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.*;

@Mapper
public interface RoleMapper extends SuperMapper<Role> {
    /**
     * 通过角色名称查询权限
     * @param roleName
     * @return
     */
//    @Select("select r.id id, r.name rolename,p.per_code per_code from t_role r LEFT JOIN t_role_permission rp " +
//            " ON rp.role_id=r.id LEFT JOIN t_permission p ON rp.permission_id=p.id where r.name=#{roleName}")
    @Select("select r.id id, r.name rolename,p.per_code  from t_role r LEFT JOIN t_role_permission rp " +
            "ON rp.role_id=r.id LEFT JOIN t_permission p ON rp.permission_id=p.id where r.name='admin'")
    List<RolePermissionVo> findByRoleName(String roleName);
}
