package com.sunxiao.jwtdemo.user.mapper;

import com.sunxiao.jwtdemo.superd.smapper.SuperMapper;
import com.sunxiao.jwtdemo.user.pojo.domain.Permission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PermissionMapper extends SuperMapper<Permission> {
}
