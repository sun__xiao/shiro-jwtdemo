package com.sunxiao.jwtdemo.user.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * UserRole
 * @author sunxiao
 * @date 2021/10/14 14:48
 */
@TableName(value = "t_user_role")
@Data
@Accessors(chain = true)//链式编程
@AllArgsConstructor
@NoArgsConstructor
public class UserRole implements Serializable {

    private static final long serialVersionUID = -3397516891053950951L;


    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private String userId;

    /**
     * 角色id
     */
    @TableField(value= "role_id")
    private String roleId;

}