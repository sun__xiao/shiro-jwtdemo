package com.sunxiao.jwtdemo.user.pojo.dto;

import com.sunxiao.jwtdemo.superd.spojo.StringSuperEntity;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import lombok.Data;

import javax.validation.constraints.NotNull;
@Data
public class UserDto extends StringSuperEntity<UserDto> {
    /**
     * 昵称
     */
    @NotNull(message = "用户名不能为空")
    private String username;
    /**
     * 性别
     */
    private int sex;
    /**
     * 帐号
     */
    @NotNull(message = "帐号不能为空")
    private String account;
}
