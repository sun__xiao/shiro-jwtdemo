package com.sunxiao.jwtdemo.user.pojo.common.httpresponse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ResponseInfo<T> {
    /**  状态码  **/
    private int code;
    /**  消息说明  **/
    private String message;
    /**  结果  **/
    private  T content;

    public ResponseInfo(){

    }
    public ResponseInfo(int code){
        this.code=code;
    }

    /**
     * 返回创建信息
     * @param httpResponseStatus http请求状态
     * @param <T> 泛型
     * @return 返回信息
     */
    public static <T> ResponseInfo<T> of(HttpResponseStatus httpResponseStatus){
            return of(httpResponseStatus.getCode(),httpResponseStatus.getMessage());
    }

    /**
     * 返回创建信息
     * @param code 状态码
     * @param message 描述
     * @param <T> 返回信息
     * @return
     */
    public static <T> ResponseInfo<T> of(int code,String message){
        return of(code,message,null);
    }

    /**
     * 返回创建信息
     * @param httpResponseStatus http请求状态
     * @param message 描述信息
     * @param <T> 泛型
     * @return
     */
    public static <T> ResponseInfo<T> of(HttpResponseStatus httpResponseStatus,String message){
        return of(httpResponseStatus.getCode(),message);
    }

    /**
     * 返回创建信息
     * @param httpResponseStatus http请求状态
     * @param t 返回值
     * @param <T> 泛型
     * @return 返回信息
     */
    public static <T> ResponseInfo<T> of(HttpResponseStatus httpResponseStatus,T t){
        return of(httpResponseStatus.getCode(),httpResponseStatus.getMessage(),t);
    }

    /**
     * 创建返回信息
     * @param code 状态码
     * @param message 描述
     * @param t 返回值
     * @param <T> 泛型
     * @return
     */
    public static <T> ResponseInfo<T> of(int code,String message,T t){
        ResponseInfo<T> responseInfo=new ResponseInfo<>();
        responseInfo.setCode(code).setMessage(message).setContent(t);
        return responseInfo;
    }

    /**
     * 创建返回信息
     * @param httpResponseStatus http请求状态
     * @param message 描述
     * @param t 返回值
     * @param <T> 泛型
     * @return
     */
    public static <T> ResponseInfo<T> of(HttpResponseStatus httpResponseStatus,String message,T t){
        return of(httpResponseStatus.getCode(),message,t);
    }

    /**
     * 请求成功返回信息
     * @param <T> 泛型
     * @return
     */
    public static <T> ResponseInfo<T> ofOk(){
        return of(HttpResponseStatus.SC_OK);
    }

    /**
     * 请求成功返回信息
     * @param t 返回数据
     * @param <T> 泛型
     * @return
     */
    public static <T> ResponseInfo<T> ofOk(T t){
        return of(HttpResponseStatus.SC_OK,t);
    }

    /**
     * 请求成功返回信息
     * @param message 描述
     * @param <T> 泛型
     * @return
     */
    public static <T> ResponseInfo<T> ofOk(String message){
        return of(HttpResponseStatus.SC_OK,message);
    }

    /**
     * 请求成功返回信息
     * @param message 描述
     * @param t 返回数据
     * @param <T> 泛型
     * @return
     */
    public static <T> ResponseInfo<T> ofOk(String message,T t){
        return of(HttpResponseStatus.SC_OK,message,t);
    }

    /**
     * 请求未找到信息
     * @param <T>
     * @return
     */
    public static <T> ResponseInfo<T> ofNotSelect(){
        return of(HttpResponseStatus.SC_NOT_SELECT);
    }

    /**
     * 请求未找到数据
     * @param message 描述
     * @param <T>
     * @return
     */
    public static <T> ResponseInfo<T> ofNotSelect(String message){
        return of(HttpResponseStatus.SC_NOT_SELECT,message);
    }

    /**
     *
     * 后续补充
     * 更新成功
     * 更新失败
     * 创建成功
     * 创建失败
     * 删除成功
     * 删除失败
     *
     *
     *
     */






}
