package com.sunxiao.jwtdemo.user.pojo.common.httpresponse;

import lombok.Getter;

@Getter
public enum HttpResponseStatus {

    /**
     * 请求状态码
     */
    SC_OK(200,"请求成功"),
    SC_CREATE(201,"创建成功"),
    SC_UPDATE(202,"修改成功"),
    SC_DELETE(203,"删除成功"),
    SC_NOT_SELECT(204,"未找到数据"),
    SC_CREATE_FAIL(301,"创建失败"),
    SC_UPDATE_FAIL(302,"修改失败"),
    SC_DELETE_FAIL(303,"删除失败");


    /**
     * 状态码
     */
    private int code;
    /**
     * 状态描述
     */
    private String message;

    HttpResponseStatus(int code,String message){
        this.code=code;
        this.message=message;
    }
}
