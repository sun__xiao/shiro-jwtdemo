package com.sunxiao.jwtdemo.user.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sunxiao.jwtdemo.superd.spojo.StringSuperEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Role
 * @author sunxiao
 * @date 2021/10/14 14:42
 */
@TableName(value = "t_role")
@Data
@Accessors(chain = true)//链式编程
@AllArgsConstructor
@NoArgsConstructor
public class Role extends StringSuperEntity<Role> {

    private static final long serialVersionUID = 6382925944937625109L;

    /**
     * 角色名称
     */
    private String name;

}