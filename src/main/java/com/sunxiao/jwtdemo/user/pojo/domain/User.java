package com.sunxiao.jwtdemo.user.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;


import com.sunxiao.jwtdemo.superd.spojo.StringSuperEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;

/**
 * User
 * @author sunxiao
 * @date 2021/10/14 14:41
 */
@TableName(value = "t_user")
@Data
@Accessors(chain = true)//链式编程
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class User extends StringSuperEntity<User> {

//    private static final long serialVersionUID = 3342723124953988435L;
    /**
     * 帐号
     */
    @NotNull(message = "帐号不能为空")
    private String account;

    /**
     * 密码
     */
    @NotNull(message = "密码不能为空")
    private String password;

    /**
     * 昵称
     */
    @NotNull(message = "用户名不能为空")
    private String username;
    /**
     * 性别
     */
    private int sex;
    /**
     * 头像地址
     */
    @TableField(value="head_url")
    private String headUrl;

    /**
     * 密码盐
     */
    private String salt;



}