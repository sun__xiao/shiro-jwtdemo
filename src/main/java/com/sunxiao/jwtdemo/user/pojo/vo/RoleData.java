package com.sunxiao.jwtdemo.user.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)//链式编程
@AllArgsConstructor
@NoArgsConstructor
public class RoleData{
    private String rolename;
    private List<RolePermissionVo> rolePermissionVos;
}
