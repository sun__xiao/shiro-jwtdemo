package com.sunxiao.jwtdemo.user.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sunxiao.jwtdemo.superd.spojo.StringSuperEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Permission
 * @author sunxiao
 * @date 2021/10/14 14:41
 */
@Data
@TableName(value = "t_permission")
@Accessors(chain = true)//链式编程
@AllArgsConstructor
@NoArgsConstructor
public class Permission extends StringSuperEntity<Permission> {

    private static final long serialVersionUID = -8834983208597107688L;
    /**
     * 资源名称
     */
    private String name;
    /**
     * 权限代码字符串
     */
    @TableField(value="per_code")
    private String perCode;

}