package com.sunxiao.jwtdemo.user.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginUserDto {
    @NotNull(message = "帐号不能为空")
    private String account;

    /**
     * 密码
     */
    @NotNull(message = "密码不能为空")
    private String password;
    /**
     * 昵称
     */
    private String username;
    /**
     * 性别
     */
    private int sex;
    /**
     * 头像地址
     */
    private String headUrl;
}
