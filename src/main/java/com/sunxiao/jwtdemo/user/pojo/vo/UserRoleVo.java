package com.sunxiao.jwtdemo.user.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)//链式编程
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleVo {
    private String account;
    private String roleid;
    private String rolename;
}
