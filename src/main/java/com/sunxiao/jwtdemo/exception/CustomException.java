package com.sunxiao.jwtdemo.exception;

/**
 * 自定义异常(CustomException)
 * @author 孙潇
 * @date 2021/10/15 10:37
 */
public class CustomException extends RuntimeException {

    public CustomException(String msg){
        super(msg);
    }

    public CustomException() {
        super();
    }
}
