package com.sunxiao.jwtdemo.exception;

/**
 * 自定义401无权限异常(UnauthorizedException)
 * @author 孙潇
 * @date 2021/10/15 11:35
 */
public class CustomUnauthorizedException extends RuntimeException {

    public CustomUnauthorizedException(String msg){
        super(msg);
    }

    public CustomUnauthorizedException() {
        super();
    }
}
