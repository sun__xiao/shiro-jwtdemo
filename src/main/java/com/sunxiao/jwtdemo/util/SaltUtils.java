package com.sunxiao.jwtdemo.util;

import java.util.Random;

public class SaltUtils {
    /**
     * 随机获取盐值
     * @param n
     * @return
     */
    public static String getSalt(int n){
        char salt[]= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*".toCharArray();
        StringBuilder stringBuilder=new StringBuilder();
        for (int i = 0; i < n; i++) {
            char c = salt[new Random().nextInt(salt.length)];
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }
}
