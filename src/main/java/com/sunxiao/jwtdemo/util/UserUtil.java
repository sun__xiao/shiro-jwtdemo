package com.sunxiao.jwtdemo.util;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sunxiao.jwtdemo.exception.CustomException;
import com.sunxiao.jwtdemo.superd.spojo.BeanConverts;
import com.sunxiao.jwtdemo.user.mapper.UserMapper;
import com.sunxiao.jwtdemo.user.pojo.common.Constant;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import com.sunxiao.jwtdemo.user.pojo.dto.UserDto;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 获取当前登录用户工具类
 *
 * @author sunxiao
 * @date 2021/10/11
 */
@Component
public class UserUtil {

    private final UserMapper userMapper;

    @Autowired
    public UserUtil(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    /**
     * 获取当前登录用户
     *
     * @param
     * @return
     * @author sunxiao
     * @date 2021/10/11
     */
    public UserDto getUser() {
        String token = SecurityUtils.getSubject().getPrincipal().toString();
        // 解密获得Account
        String account = JwtUtil.getClaim(token, Constant.ACCOUNT);

        User user = userMapper.selectOne(new QueryWrapper<User>().eq("account", account));
        UserDto convert = BeanConverts.convert(user, UserDto.class);
        // 用户是否存在
        if (user == null) {
            throw new CustomException("该帐号不存在(The account does not exist.)");
        }
        return convert;
    }

    /**
     * 获取当前登录用户Id
     *
     * @param
     * @return
     * @author sunxiao
     * @date 2021/10/11
     */
    public String getUserId() {
        return getUser().getId();
    }

    /**
     * 获取当前登录用户Token
     *
     * @param
     * @return
     * @author sunxiao
     * @date 2021/10/11
     */
    public String getToken() {
        return SecurityUtils.getSubject().getPrincipal().toString();
    }

    /**
     * 获取当前登录用户Account
     *
     * @param
     * @return
     * @author sunxiao
     * @date 2021/10/11
     */
    public String getAccount() {
        String token = SecurityUtils.getSubject().getPrincipal().toString();
        // 解密获得Account
        return JwtUtil.getClaim(token, Constant.ACCOUNT);
    }
}
