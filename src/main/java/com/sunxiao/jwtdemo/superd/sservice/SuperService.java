package com.sunxiao.jwtdemo.superd.sservice;

import com.sunxiao.jwtdemo.superd.spojo.SuperEntity;

import java.io.Serializable;

public interface SuperService<T extends SuperEntity<T,PK>,PK extends Serializable> extends MybatisService<T,PK> {

    /**
     * 单字段查询 eq
     * @param function 字段
     * @param object 字段值
     * @return 集合
     */
//    Optional<T> oneByFieldEquals(SFunction<T, String> function, Object object); //lambda表达式使用

    //使用
//    public List<T> chaxundangeziduan(String ziduan){
//        return xxx.oneByFieldEquals(实体类::getcode(),实体类).orElseGet(ArrayList::new);
//    }
}
