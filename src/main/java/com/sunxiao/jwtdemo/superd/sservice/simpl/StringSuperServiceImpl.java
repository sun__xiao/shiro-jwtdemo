package com.sunxiao.jwtdemo.superd.sservice.simpl;


import com.sunxiao.jwtdemo.superd.spojo.StringSuperEntity;
import com.sunxiao.jwtdemo.superd.sservice.StringSuperService;

public abstract class StringSuperServiceImpl<T extends StringSuperEntity<T>> extends SuperServiceImpl<T,String> implements StringSuperService<T> {
}
