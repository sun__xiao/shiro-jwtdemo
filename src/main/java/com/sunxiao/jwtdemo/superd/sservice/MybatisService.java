package com.sunxiao.jwtdemo.superd.sservice;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sunxiao.jwtdemo.superd.spojo.SuperEntity;


import java.io.Serializable;
import java.util.List;

public interface MybatisService<T extends SuperEntity<T,PK>,PK extends Serializable> {
    /**
     * 创建查询条件
     * @return
     */
    QueryWrapper<T> createQueryWrapper();

    /**
     * 创建查询条件
     * @param pageable 分页条件
     * @return
     */
    QueryWrapper<T> createQueryWrapper(Page pageable);

    /**
     * 查询条件的
//     * @param baseSearchable
     * @return
     */
//    QueryWrapper<T> createQueryWrapper(BaseSearchable baseSearchable);

    int insert(T entity);

    /**
     * 批量插入
     * @param entityList
     * @return
     */
    int insertBatchSomeColunm(List<T> entityList);


    int deleteById(PK id);

    /**
     * 条件删除
     * @param wrapper
     * @return
     */
    int delete(Wrapper<T> wrapper);


    int updateById(PK id);

    /**
     * 条件更新
     * @param wrapper
     * @return
     */
    int updateById(Wrapper<T> wrapper);

    int update(T entity, Wrapper<T> updateWrapper);

    T selectById(PK id);

    /**
     * 条件查询一个
     * @param wrapper
     * @return
     */
    T selectOne(Wrapper<T> wrapper);

    List<T> selectList(Wrapper<T> wrapper);

    /**
     * 分页查询
     * @param
     * @return
     */
    IPage<T> selectMyPage(Page<T> page, Wrapper<T> queryWrapper);

}
