package com.sunxiao.jwtdemo.superd.sservice;


import com.sunxiao.jwtdemo.superd.spojo.StringSuperEntity;

public interface StringSuperService<T extends StringSuperEntity<T>> extends SuperService<T,String>{
}
