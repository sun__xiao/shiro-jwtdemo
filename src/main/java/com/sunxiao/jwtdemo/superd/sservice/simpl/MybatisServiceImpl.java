package com.sunxiao.jwtdemo.superd.sservice.simpl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sunxiao.jwtdemo.superd.smapper.SuperMapper;
import com.sunxiao.jwtdemo.superd.spojo.SuperEntity;
import com.sunxiao.jwtdemo.superd.sservice.MybatisService;


import java.io.Serializable;
import java.util.List;

public abstract class MybatisServiceImpl<T extends SuperEntity<T,PK>,PK extends Serializable> implements MybatisService<T,PK> {

    public abstract SuperMapper<T> getSuperMapper();
    @Override
    public QueryWrapper<T> createQueryWrapper() {
        return new QueryWrapper<>();
    }

    @Override
    public QueryWrapper<T> createQueryWrapper(Page page) {
        return createQueryWrapper(page);
    }



    @Override
    public int insert(T entity) {
        return getSuperMapper().insert(entity);
    }

    @Override
    public int insertBatchSomeColunm(List<T> entityList) {
        return getSuperMapper().insertBatchSomeColunm(entityList);
    }

    @Override
    public int deleteById(PK id) {
        return getSuperMapper().deleteById(id);
    }

    @Override
    public int delete(Wrapper<T> wrapper) {
        return getSuperMapper().delete(wrapper);
    }

    @Override
    public int updateById(PK id) {
        return getSuperMapper().updateById((T) id);
    }
    @Override
    public int update(T entity, Wrapper<T> updateWrapper) {
        return getSuperMapper().update(entity,updateWrapper);
    }

    @Override
    public int updateById(Wrapper<T> wrapper) {
        return getSuperMapper().updateById(wrapper);
    }

    @Override
    public T selectById(PK id) {
        return getSuperMapper().selectById(id);
    }

    @Override
    public T selectOne(Wrapper<T> wrapper) {
        return getSuperMapper().selectOne(wrapper);
    }

    @Override
    public List<T> selectList(Wrapper<T> wrapper) {
        return getSuperMapper().selectList(wrapper);
    }
    public IPage<T> selectMyPage(Page<T> page, Wrapper<T> queryWrapper){
        return getSuperMapper().selectPage(page,queryWrapper);
    };
}
