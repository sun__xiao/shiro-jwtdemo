package com.sunxiao.jwtdemo.superd.sservice.simpl;



import com.sunxiao.jwtdemo.superd.spojo.SuperEntity;
import com.sunxiao.jwtdemo.superd.sservice.SuperService;

import java.io.Serializable;

public abstract class SuperServiceImpl<T extends SuperEntity<T,PK>,PK extends Serializable>extends MybatisServiceImpl<T,PK> implements SuperService<T,PK> {
}
