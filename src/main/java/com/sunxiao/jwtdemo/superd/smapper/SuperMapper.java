package com.sunxiao.jwtdemo.superd.smapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sunxiao.jwtdemo.superd.spojo.SuperEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SuperMapper<T extends SuperEntity> extends BaseMapper<T> {
    int insertBatchSomeColunm(List<T> entityList);
    int updateById(Wrapper<T> wrapper);

//
}
