package com.sunxiao.jwtdemo.superd.spojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import jdk.nashorn.internal.codegen.types.BooleanType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@ToString(callSuper = true)
@Setter
public abstract class SuperEntity<T,PK extends Serializable> implements Serializable {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",position = 1)
//    private PK id;
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",position = 2)
    @TableField(fill = FieldFill.INSERT,value = "create_at")
    private LocalDateTime createAt;
    @ApiModelProperty(value = "更新时间",position = 3)
    @TableField(fill = FieldFill.INSERT_UPDATE,value = "modified_at")
    private LocalDateTime modifiedAt;
    @TableField(fill = FieldFill.INSERT,value = "create_by")
    @ApiModelProperty(value = "创建人",position = 4)
    private String createBy;
    @TableField(fill = FieldFill.INSERT_UPDATE,value = "modified_by")
    @ApiModelProperty(value = "更新人",position = 5)
    private String modifiedBy;
    @ApiModelProperty(value = "版本（乐观锁）",position = 6)
    private Integer version;
    @ApiModelProperty(value = "逻辑删除",position = 7)
    @TableField(value = "is_delete")
    private BooleanType isDelete;

//
//    public T setId(PK id){
//        this.id=id;
//        return (T) this;
//    }
}
