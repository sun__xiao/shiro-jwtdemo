package com.sunxiao.jwtdemo.superd.spojo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.NonNull;
import org.springframework.beans.BeanUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class BeanConverts {
    private BeanConverts(){

    }

    public static void copyPropertiesIgnoreNull(@NonNull  Object source,@NonNull Object target,String... ignoreProperties){
        Set<String> ignoreSet=new HashSet<>();
        if (ignoreProperties!=null){
            ignoreSet.addAll(Arrays.asList(ignoreProperties));
        }
//        ignoreSet.addAll()
    }
    /**
     *  bean 转换
     * @param object 源对象
     * @param tClass 目标类型
     * @param <T> 泛型
     * @return 目标对象
     */
    public static <T> T convert(Object object,@NonNull Class<T> tClass){
        if (object==null){
            return null;
        }
        T t= BeanUtils.instantiateClass(tClass);
        BeanUtils.copyProperties(object,t);
        return t;
    }
    /**
     *  bean 转换
     * @param optional 源对象
     * @param tClass 目标类型
     * @param <T> 泛型
     * @return 目标对象
     */
    public static <T>Optional<T> convert(@NonNull Optional<?> optional,@NonNull Class<T> tClass){
        return optional.map(obj->convert(obj,tClass));
    }
    /**
     *  bean 集合转换
     * @param collection 源对象
     * @param tClass 目标类型
     * @param <T> 泛型
     * @return 目标对象集合
     */
    public static <T>List<T> convert(@NonNull Collection<?> collection,@NonNull Class<T> tClass){
        return collection.stream().map(item->convert(item,tClass)).collect(Collectors.toList());
    }
    /**
     *  bean 集合转换
     * @param iterable 实体集合
     * @param tClass 目标类型
     * @param <T> 泛型
     * @return 目标对象集合
     */
    public static <T>List<T> convert(@NonNull Iterable<?> iterable,@NonNull Class<T> tClass){
        return convert(StreamSupport.stream(iterable.spliterator(),false),tClass);
    }

    /**
     * bean 集合转换
     * @param stream 实体集合流
     * @param tClass 目标类型
     * @param <T>泛型
     * @return 目标对象集合
     */

    public static <T>List<T> convert(@NonNull Stream<?> stream,@NonNull Class<T> tClass){
        return stream.map(entity->convert(entity,tClass)).collect(Collectors.toList());
    }

    /**
     * 实体分页转换
     * @param page 实体分页
     * @param tClass 目标对象
     * @param <T> 泛型
     * @return 目标分页
     */
//    public static <T> Page<T> convert(@NonNull Page<?> page, @NonNull Class<T> tClass){
//        List<T> list=convert(page.getContent(),tClass);
//        return new PageImpl<>(list,page.getPageable(),page.getTotalElements());
//    }

    /**
     * map转对象
     * @param clazz 目标类型
     * @param map 键值对
     * @param <T> 泛型
     * @return 目标对象
     */
    public static <T> T mapToObj(@NonNull Class<T> clazz,@NonNull Map<String,Object> map){
        T d= BeanUtils.instantiateClass(clazz);
        BeanInfo beanInfo;
        try {
            beanInfo= Introspector.getBeanInfo(d.getClass());
        } catch (IntrospectionException e) {
            e.printStackTrace();
            return null;
        }
        PropertyDescriptor[] propertyDescriptors=beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            Method setter=propertyDescriptor.getWriteMethod();
            if (setter==null){
                continue;
            }
            Object value=map.get(propertyDescriptor.getName());
            if (value==null){
                continue;
            }
            try {
                setter.invoke(d,value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return d;
    }
}
