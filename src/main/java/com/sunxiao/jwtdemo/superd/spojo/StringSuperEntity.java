package com.sunxiao.jwtdemo.superd.spojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
//@KeySequence(clazz = String.class)
public abstract class StringSuperEntity<T>  extends SuperEntity<T,String> {

}
