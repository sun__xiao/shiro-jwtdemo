package com.sunxiao.jwtdemo.shiro;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sunxiao.jwtdemo.shiro.jwt.JwtToken;
import com.sunxiao.jwtdemo.user.mapper.PermissionMapper;
import com.sunxiao.jwtdemo.user.mapper.RoleMapper;
import com.sunxiao.jwtdemo.user.mapper.UserMapper;
import com.sunxiao.jwtdemo.user.pojo.common.Constant;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import com.sunxiao.jwtdemo.user.pojo.vo.AuthenticationVo;
import com.sunxiao.jwtdemo.user.pojo.vo.RoleData;
import com.sunxiao.jwtdemo.user.service.PermissionService;
import com.sunxiao.jwtdemo.user.service.RoleService;
import com.sunxiao.jwtdemo.user.service.UserService;
import com.sunxiao.jwtdemo.util.JedisUtil;
import com.sunxiao.jwtdemo.util.JwtUtil;
import com.sunxiao.jwtdemo.util.common.StringUtil;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 自定义Realm
 * @author 孙潇
 * @date 2021/10/15 10:35
 */
@Service
public class UserRealm extends AuthorizingRealm {

    private final UserMapper userMapper;
    private final RoleMapper roleMapper;
    private final PermissionMapper permissionMapper;

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    PermissionService permissionService;
    @Autowired
    public UserRealm(UserMapper userMapper, RoleMapper roleMapper, PermissionMapper permissionMapper) {
        this.userMapper = userMapper;
        this.roleMapper = roleMapper;
        this.permissionMapper = permissionMapper;
    }

    /**
     * 大坑，必须重写此方法，不然Shiro会报错
     */
    @Override
    public boolean supports(AuthenticationToken authenticationToken) {
        return authenticationToken instanceof JwtToken;
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        String account = JwtUtil.getClaim(principalCollection.toString(), Constant.ACCOUNT);

        String json = JedisUtil.getJson(Constant.PREFIX_SHIRO_AUTH + account);

        if (StringUtil.isNotBlank(json)) {//redis取值
            AuthenticationVo authenticationVo = JSON.parseObject(json, AuthenticationVo.class);
            List<RoleData> rolenameList = authenticationVo.getRolenameList();
            rolenameList.stream().forEach(e->{
                simpleAuthorizationInfo.addRole(e.getRolename());
                e.getRolePermissionVos().stream().forEach(rp->{
                    simpleAuthorizationInfo.addStringPermission(rp.getPerCode());
                });
            });
        }else{//数据库取值
            AuthenticationVo authenticationByAccount = userService.findAuthenticationByAccount(account);
            List<RoleData> rolenameList = authenticationByAccount.getRolenameList();
            rolenameList.stream().forEach(e->{
                //添加角色
                simpleAuthorizationInfo.addRole(e.getRolename());
                e.getRolePermissionVos().stream().forEach(rp->{
                    //添加权限
                    simpleAuthorizationInfo.addStringPermission(rp.getPerCode());
                });
            });
        //鉴权以后把数据覆盖redis
            String redisData = JSON.toJSONString(authenticationByAccount);
            JedisUtil.setJson(Constant.PREFIX_SHIRO_AUTH + account,redisData);
        }
        return simpleAuthorizationInfo;
    }

    /**
     * 默认使用此方法进行用户名正确与否验证，错误抛出异常即可。
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String token = (String) authenticationToken.getCredentials();
        // 解密获得account，用于和数据库进行对比
        String account = JwtUtil.getClaim(token, Constant.ACCOUNT);
        // 帐号为空
        if (StringUtil.isBlank(account)) {
            throw new AuthenticationException("Token中帐号为空(The account in Token is empty.)");
        }
        // 查询用户是否存在
        User user = userService.selectOne(new QueryWrapper<User>().eq("account", account));
        if (user == null) {
            throw new AuthenticationException("该帐号不存在(The account does not exist.)");
        }
        // 开始认证，要AccessToken认证通过，且Redis中存在RefreshToken，且两个Token时间戳一致
        if (JwtUtil.verify(token) && JedisUtil.exists(Constant.PREFIX_SHIRO_REFRESH_TOKEN + account)) {
            // 获取RefreshToken的时间戳
            String currentTimeMillisRedis = JedisUtil.getObject(Constant.PREFIX_SHIRO_REFRESH_TOKEN + account).toString();
            // 获取AccessToken时间戳，与RefreshToken的时间戳对比
            if (JwtUtil.getClaim(token, Constant.CURRENT_TIME_MILLIS).equals(currentTimeMillisRedis)) {
                //添加redis权限缓存
                AuthenticationVo authenticationByAccount = userService.findAuthenticationByAccount(account);
                String redisData = JSON.toJSONString(authenticationByAccount);
                JedisUtil.setJson(Constant.PREFIX_SHIRO_AUTH + account,redisData);
                return new SimpleAuthenticationInfo(token, token, "userRealm");
            }
        }
        throw new AuthenticationException("Token已过期(Token expired or incorrect.)");
    }
}
