package com.sunxiao.jwtdemo;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sunxiao.jwtdemo.user.pojo.domain.User;
import com.sunxiao.jwtdemo.user.pojo.vo.AuthenticationVo;
import com.sunxiao.jwtdemo.user.service.UserService;
import com.sunxiao.jwtdemo.util.AesCipherUtil;
import com.sunxiao.jwtdemo.util.JedisUtil;
import com.sunxiao.jwtdemo.util.common.StringUtil;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.Valid;
import java.util.List;

@SpringBootTest
class JwtdemoApplicationTests {
    @Autowired
    UserService service;

    @Test
    void contextLoads() {
//        for (int i = 0; i < 10; i++) {
//            User user=new User();
//            user.setAccount("sunxiao"+i).setPassword("123456").setSex(1).setHeadUrl("localhost:8080");
//            service.insert(user);
//        }

        Page page=new Page();
        page.setSize(2);
        page.setPages(1);
        IPage<User> userIPage = service.selectMyPage(new Page<User>(1,2),
                new QueryWrapper<User>().orderByDesc("username"));
        System.out.println(userIPage.getRecords().toString());
    }
    @Test
    void DESss(){
        String password="123456";
        String salt="123";
        int num=1245;
        String md5Hash=new Md5Hash(password, salt,  num).toBase64();
        String s = AesCipherUtil.enCryptoMd5(md5Hash,salt);
        System.out.println(s);
        System.out.println("ODZCRDYxQTJGMTg5NkNEOTE3REZENEYxMzE2MTZEQjZFQkE2Nzk2MEEyMDM5M0U3MkExMkE1ODZBMjgyN0I4Mw==".equals("ODZCRDYxQTJGMTg5NkNEOTE3REZENEYxMzE2MTZEQjZFQkE2Nzk2MEEyMDM5M0U3MkExMkE1ODZBMjgyN0I4Mw=="));
//        ODZCRDYxQTJGMTg5NkNEOTE3REZENEYxMzE2MTZEQjZFQkE2Nzk2MEEyMDM5M0U3MkExMkE1ODZBMjgyN0I4Mw==
//        RkZBREUzMjQxMjVBNEYyQTZCRjhDMTIxRkNDQjNGMkYxMkZBRUFBRDU2NjJDRTQyNEJCRDBFQzdDNjMyNzc4Nw==
//        SimpleAuthenticationInfo simpleAuthenticationInfo=new SimpleAuthenticationInfo(
//                userCode,pwd,ByteSource.Util.bytes(salt),this.getName());
    }
    @Test
    void RoleTest(){
        AuthenticationVo sunxiao1 = service.findAuthenticationByAccount("sunxiao1");
        //对象转化为json存redis
        String json= JSON.toJSONString(sunxiao1);
        JedisUtil.setJson("ceshik",json);
        String ceshik = JedisUtil.getJson("ceshik");
        System.out.println(ceshik);
        //取redis json数据转化为对象
        AuthenticationVo authenticationVo = JSON.parseObject(ceshik, AuthenticationVo.class);
        System.out.println(authenticationVo);

        String ceshika = JedisUtil.getJson("ceshikdasda");
        System.out.println(StringUtil.isBlank(ceshika));
    }

}
